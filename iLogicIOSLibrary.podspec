#
# Be sure to run `pod lib lint iLogicIOSLibrary.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'iLogicIOSLibrary'
  s.version          = '1.11'
  s.summary          = 'A short description of iLogicIOSLibrary.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/binhduong_ilogic/ilogicioslibrary'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Binh.Duong' => 'binh.duong@innotech.vn' }
  s.source           = { :git => 'https://binhduong_ilogic@bitbucket.org/binhduong_ilogic/ilogicioslibrary.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'iLogicIOSLibrary/Classes/**/*'
  
  # s.resource_bundles = {
  #   'iLogicIOSLibrary' => ['iLogicIOSLibrary/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.vendored_frameworks = 'iLogicIOSLibrary.framework'
  s.dependency 'Alamofire', '4.9.0'
  s.dependency 'SDWebImage', '= 5.2.3'
  s.dependency 'PodAsset'
  s.dependency 'SwiftyJSON', '= 5.0.0'
  s.dependency 'IQKeyboardManager'
  s.dependency 'SocketRocket'
  s.dependency 'FSCalendar'
  s.dependency 'EFQRCode', '~> 4.2.1'
  s.dependency 'KRProgressHUD', '3.4.0'
end
